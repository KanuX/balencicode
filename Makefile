# CORE (needed to compile)

ifndef CC
$(warning C compiler not detected. Setting default compiler to GCC...)
	CC :=	gcc
endif
ifndef CXX
$(warning C++ compiler not detected. Setting default compiler to GCC...)
	CXX := g++
endif

# CORE OPTIONAL (compiler flags)

ifndef CFLAGS
	CFLAGS :=
endif
ifndef CXXFLAGS
	CXXFLAGS :=
endif

# EXTRA OPTIONAL (flags)

ifndef ARCH
	ARCH :=
endif
ifndef ARCHFLAGS
	ARCHFLAGS :=
endif
ifndef HOSTCC
	HOSTCC :=
endif
ifndef HOSTCXX
	HOSTCXX :=
endif
ifndef LD
	LD :=
endif
ifndef HOSTLD
	HOSTLD :=
endif
ifndef AR
	AR :=
endif
ifndef HOSTAR
	HOSTAR :=
endif
ifndef NM
	NM :=
endif
ifndef STRIP
	STRIP :=
endif
ifndef OBJCOPY
	OBJCOPY :=
endif
ifndef OBJDUMP
	OBJDUMP :=
endif
ifndef READELF
	READELF :=
endif

# Append to 'CFLAGS'/'CXXFLAGS' if defined

ifdef ARCH
	CFLAGS := $(CFLAGS) ARCH=$(ARCH)
	CXXFLAGS := $(CXXFLAGS) ARCH=$(ARCH)
endif
ifdef ARCHFLAGS
	CFLAGS := $(CFLAGS) ARCHFLAGS=$(ARCHFLAGS)
	CXXFLAGS := $(CXXFLAGS) ARCHFLAGS=$(ARCHFLAGS)
endif
ifdef HOSTCC
	CFLAGS := $(CFLAGS) HOSTCC=$(HOSTCC)
	CXXFLAGS := $(CXXFLAGS) HOSTCC=$(HOSTCC)
endif
ifdef HOSTCXX
	CFLAGS := $(CFLAGS) HOSTCXX=$(HOSTCXX)
	CXXFLAGS := $(CXXFLAGS) HOSTCXX=$(HOSTCXX)
endif
ifdef LD
	CFLAGS := $(CFLAGS) LD=$(LD)
	CXXFLAGS := $(CXXFLAGS) LD=$(LD)
endif
ifdef HOSTLD
	CFLAGS := $(CFLAGS) HOSTLD=$(HOSTLD)
	CXXFLAGS := $(CXXFLAGS) HOSTLD=$(HOSTLD)
endif
ifdef AR
	CFLAGS := $(CFLAGS) AR=$(AR)
	CXXFLAGS := $(CXXFLAGS) AR=$(AR)
endif
ifdef HOSTAR
	CFLAGS := $(CFLAGS) HOSTAR=$(HOSTAR)
	CXXFLAGS := $(CXXFLAGS) HOSTAR=$(HOSTAR)
endif
ifdef NM
	CFLAGS := $(CFLAGS) NM=$(NM)
	CXXFLAGS := $(CXXFLAGS) NM=$(NM)
endif
ifdef STRIP
	CFLAGS := $(CFLAGS) STRIP=$(STRIP)
	CXXFLAGS := $(CXXFLAGS) STRIP=$(STRIP)
endif
ifdef OBJCOPY
	CFLAGS := $(CFLAGS) OBJCOPY=$(OBJCOPY)
	CXXFLAGS := $(CXXFLAGS) OBJCOPY=$(OBJCOPY)
endif
ifdef OBJDUMP
	CFLAGS := $(CFLAGS) OBJDUMP=$(OBJDUMP)
	CXXFLAGS := $(CXXFLAGS) OBJDUMP=$(OBJDUMP)
endif
ifdef READELF
	CFLAGS := $(CFLAGS) READELF=$(READELF)
	CXXFLAGS := $(CXXFLAGS) READELF=$(READELF)
endif

# ACTION definitions

ifndef BUILD_TYPE
	BUILD_TYPE = Release
endif

# TinyCC have different flag calls compared to other compilers
ifeq ($(CC), tcc)
	CFLAGS +=
else
	CFLAGS +=
endif

ifeq ($(BUILD_TYPE), Release)
	CFLAGS += -O3
	CXXFLAGS += -O3
else
	CFLAGS += -O0 -g3 -ggdb -Wall
	CXXFLAGS += -O0 -g3 -ggdb -Wall
endif

ESCAPE        := \033
RED           := $(ESCAPE)[31m
BOLD_RED      := $(ESCAPE)[1;31m
GREEN         := $(ESCAPE)[32m
BOLD_GREEN    := $(ESCAPE)[1;32m
YELLOW        := $(ESCAPE)[33m
BOLD_YELLOW   := $(ESCAPE)[1;33m
BLUE          := $(ESCAPE)[34m
BOLD_BLUE     := $(ESCAPE)[1;34m
PURPLE        := $(ESCAPE)[35m
BOLD_PURPLE   := $(ESCAPE)[1;35m
CYAN          := $(ESCAPE)[36m
BOLD_CYAN     := $(ESCAPE)[1;36m
RESET_COLOUR  := $(ESCAPE)[0m

PWD           := $(shell pwd)
OBJ_DIR       := $(PWD)/obj
SRC_DIR       := $(PWD)/src
BIN_DIR       := $(PWD)/bin
DIRS          := bin/ obj/
HEADERS       :=
LIBRARIES     :=

# Detect MinGW differences
ifndef MINGW_CHOST
	LIBRARIES   +=
else
	LIBRARIES   +=
endif

_TARGETS      := binary_name
# TODO: Make '_SOURCES' and '_OBJECTS' be the same, since it only changes the extension.
_SOURCES      :=
_OBJECTS      :=

TARGETS       := $(addprefix $(BIN_DIR)/, $(_TARGETS))
SOURCES       := $(addprefix $(SRC_DIR)/, $(_SOURCES))
OBJECTS       := $(addprefix $(OBJ_DIR)/, $(_OBJECTS))

.PHONY: all

all: directories $(TARGETS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	@printf "[CC] ""$(GREEN)""Building object '$<'""$(RESET_COLOUR)""\n"
	@$(CC) $(CFLAGS) $(HEADERS) -c $< -o $@

$(TARGETS): $(OBJECTS)
	@printf "[BIN] ""$(BOLD_GREEN)""Linking binary '$@'""$(RESET_COLOUR)""\n"
	@$(CC) $^ $(LIBRARIES) $(HEADERS) -o $@

directories: $(DIRS)

$(DIRS):
	@printf "[DIR] ""$(BLUE)""Directory ""$(BOLD_BLUE)""'$@'""$(RESET_COLOUR)$(BLUE)"" created""$(RESET_COLOUR).""\n"
	@mkdir -p $(PWD)/$@

clean:
	@rm -rv $(BIN_DIR) 2>/dev/null || true
	@rm -rv $(OBJ_DIR) 2>/dev/null || true

