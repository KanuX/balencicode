syntax on
set number
set cursorline
set tabstop=2
set shiftwidth=2
set expandtab
colorscheme habamax

" Functions

" int: Toggle background transparency
let t:bTransparent = 0
function! s:toggleTransparency()
  if t:bTransparent == 0
    hi Normal guibg=NONE ctermbg=NONE
    let t:bTransparent = 1
  else
    hi Normal guibg=#1C1C1C ctermbg=black
    set background=dark
    let t:bTransparent = 0
  endif
endfunction
nnoremap <F10> :call <SID>toggleTransparency()<CR>

" int: Toggle TAB with spaces
let t:bSpaceTAB = 1
function! s:toggleSpaceTAB()
  if t:bSpaceTAB == 0
    set expandtab
    let t:bSpaceTAB = 1
  else
    set expandtab!
    let t:bSpaceTAB = 0
  endif
endfunction
nnoremap <F9> :call <SID>toggleSpaceTAB()<CR>
