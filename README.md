BalenciCode
===========

Did you ever wanted to be a top model coder with a different style?<br>
BalenciCode will get you covered, with good templates for your code tree.

Copy and Paste
--------------

That is all you need to do. This repository includes:

|	File			|		Description													|
|-----------|---------------------------------------|
|	Makefile	|	Tries to look like CMake							|

Links
-----

[Official](https://gitlab.com/KanuX/BalenciCode) repository.
[GitHub](https://github.com/KanuX-14/BalenciCode) repository.
